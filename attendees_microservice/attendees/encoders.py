from .models import Attendee, ConferenceVO, AccountVO
from common.json import ModelEncoder


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        count = len(AccountVO.objects.filter(email=o.email))
        # Get the count of AccountVO objects with email equal to o.email
        if count > 0:
        # Return a dictionary with "has_account": True if count > 0
            return {"has_account": True}
        # Otherwise, return a dictionary with "has_account": False
        else:
            return {"has_account": False}
